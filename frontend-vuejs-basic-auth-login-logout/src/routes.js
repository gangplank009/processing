import Vue from "vue";
import Router from "vue-router";
import BackendService from "./service/BackendService";

Vue.use(Router);

const router = new Router({
    mode: 'history', // Use browser history
    routes: [
        {
            path: "/",
            name: "Login",
            component: () => import("./components/Login"),
        },
        {
            path: "/login",
            name: "Login",
            component: () => import("./components/Login"),
        },
        {
            path: "/registration",
            name: "Registration",
            component: () => import("./components/Registration"),
        },
        {
            path: "/account",
            name: "Account",
            component: () => import("./components/Account"),
            beforeEnter: (to, from, next) => {
                if (BackendService.isUserLoggedIn()) {
                    next()
                } else {
                    next({ path: '/login'})
                }
            }
        },
        {
            path: "/logout",
            name: "Logout",
            component: () => import("./components/Logout"),
            beforeEnter: (to, from, next) => {
                BackendService.logout();
                next();
            },

        },
    ]
});

export default router;