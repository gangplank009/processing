import axios from 'axios'

const API_URL = 'http://localhost:8080';
const REPORT_URL = 'http://localhost:8083/report';
const SECURITY_URL = 'http://localhost:8082/security';
export const USER_ID_SESSION_ATTRIBUTE_NAME = 'userId';
export const USER_TOKEN_SESSION_ATTRIBUTE_NAME = 'token';
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';

class BackendService {

    loginUser(username, password) {
        return axios.post(
            `${SECURITY_URL}/login`,
            {
                username: username,
                password: password
            },
            null)
    }

    registerUser(username, password) {
        return axios.post(
            `${SECURITY_URL}/registration`,
            {
                username: username,
                password: password
            },
            null)
    }

    getUserWalletInfo(idUser, token) {
        return axios.get(
            `${API_URL}/${idUser}/wallet`,
            {
                headers: {
                    authorization: token
                }
            })
    }

    getUserWalletHistory(idUser, token) {
        return axios.get(`${API_URL}/${idUser}/history`,
            {
                headers: {
                    authorization: token
                }
            })
    }

    getUserWalletNumbers(token) {
        return axios.get(`${REPORT_URL}/wallets`,
            {
                headers: {
                    authorization: token
                }
            })
    }


    createJWT(token) {
        return 'Bearer'.concat(' ', token);
    }

    processTransferOperation(idUser, operations, token) {
        return axios.post(
            `${API_URL}/${idUser}/operations`,
            operations,
            {
                headers: {
                    authorization: token
                }
            })
    }


    registerSuccessfulLogin(userId, username, token) {
        sessionStorage.setItem(USER_ID_SESSION_ATTRIBUTE_NAME, userId);
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
        sessionStorage.setItem(USER_TOKEN_SESSION_ATTRIBUTE_NAME, 'Bearer ' + token);
    }

    logout() {
        sessionStorage.removeItem(USER_ID_SESSION_ATTRIBUTE_NAME);
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        sessionStorage.removeItem(USER_TOKEN_SESSION_ATTRIBUTE_NAME);
    }

    isUserLoggedIn() {
        let id = sessionStorage.getItem(USER_ID_SESSION_ATTRIBUTE_NAME);
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        let token = sessionStorage.getItem(USER_TOKEN_SESSION_ATTRIBUTE_NAME);
        if (user === null || id === null || token === null) return false;
        return true;
    }

    getLoggedInUserName() {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) return '';
        return user;
    }

    setupAxiosInterceptors(token) {
        axios.interceptors.request.use(
            (config) => {
                if (this.isUserLoggedIn()) {
                    config.headers.authorization = 'Bearer ' + token;
                }
                return config
            }
        )
    }
}

export default new BackendService()