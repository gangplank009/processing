package com.gangplank009.processing.interceptor;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.util.Map;

public class RestTempHeaderRequestInterceptor implements ClientHttpRequestInterceptor {

    private final Map<String, String> headers;

    public RestTempHeaderRequestInterceptor(Map<String, String> headers) {
        this.headers = headers;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpHeaders requestHeaders = request.getHeaders();
        for (String headerName : headers.keySet()) {
            requestHeaders.set(headerName, headers.get(headerName));
        }
        return execution.execute(request, body);
    }
}
