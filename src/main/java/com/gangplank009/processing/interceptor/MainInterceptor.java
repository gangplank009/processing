package com.gangplank009.processing.interceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

@Component
public class MainInterceptor implements HandlerInterceptor {

    @Value(value = "${jwt.header}")
    private String STR_AUTH;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Enumeration<String> headerNames = request.getHeaderNames();
        String strToken = "";
        while(headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            System.out.print(headerName + " : ");
            System.out.print(request.getHeader(headerName));
            System.out.println();
            if (STR_AUTH.equalsIgnoreCase(headerName))
                strToken = request.getHeader(headerName);
        }
        response.setHeader(STR_AUTH, strToken);

        /*
        JwtAuthentication jwt;
        Principal userPrincipal = request.getUserPrincipal();
        if (userPrincipal instanceof JwtAuthentication) {
            jwt = (JwtAuthentication) userPrincipal;
            System.out.println(jwt.getId());
            System.out.println(jwt.getName());
            System.out.println(jwt.isAuthenticated());
        }*/

        return true;
    }
}
