package com.gangplank009.processing.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Configuration
public class RestTemplateConfiguration {

    @Value("${url.local}")
    private String localURL;
    @Value("${url.azat}")
    private String azatURL;

    @Value("${port.security}")
    private String portSecurity;
    @Value("${port.processing}")
    private String portProcessing;
    @Value("${port.report}")
    private String portReport;

    @Value("${urn.report.wallet}")
    private String reportWalletURN;
    @Value("${urn.report}")
    private String reportURN;
    @Value("${urn.security}")
    private String securityURN;


    @Bean
    public RestTemplate reportWalletTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(localURL + portReport + reportWalletURN));
        return restTemplate;
    }

    @Bean
    public RestTemplate reportTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(localURL + portReport + reportURN));
        return restTemplate;
    }

    @Bean
    public RestTemplate securityTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(localURL + portSecurity + securityURN));
        return restTemplate;
    }
}
