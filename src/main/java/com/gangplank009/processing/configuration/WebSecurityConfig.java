package com.gangplank009.processing.configuration;

import com.gangplank009.processing.security.JwtAuthEntryPoint;
import com.gangplank009.processing.security.JwtAuthProvider;
import com.gangplank009.processing.security.JwtAuthTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtAuthEntryPoint unauthorizedHandler;

    private final JwtAuthProvider jwtAuthProvider;

    public WebSecurityConfig(JwtAuthEntryPoint unauthorizedHandler, JwtAuthProvider jwtAuthProvider) {
        this.unauthorizedHandler = unauthorizedHandler;
        this.jwtAuthProvider = jwtAuthProvider;
    }

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) {
        authenticationManagerBuilder.authenticationProvider(jwtAuthProvider);
    }

    @Bean
    public JwtAuthTokenFilter authenticationFilterBean() throws Exception {
        return new JwtAuthTokenFilter();
    }

    @Bean
    public FilterRegistrationBean<JwtAuthTokenFilter> filterRegistrationBean(@Autowired JwtAuthTokenFilter jwtAuthTokenFilter) {
        final FilterRegistrationBean<JwtAuthTokenFilter> filterFilterRegistrationBean = new FilterRegistrationBean<>();
        filterFilterRegistrationBean.setFilter(jwtAuthTokenFilter);
        filterFilterRegistrationBean.setEnabled(false);
        return filterFilterRegistrationBean;
    }


    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .anyRequest().authenticated();

        httpSecurity.addFilterBefore(authenticationFilterBean(), UsernamePasswordAuthenticationFilter.class);
        httpSecurity.headers().cacheControl();
    }
}
