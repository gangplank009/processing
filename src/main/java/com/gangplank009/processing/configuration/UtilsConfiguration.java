package com.gangplank009.processing.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class UtilsConfiguration {

    @Bean
    public Random randomLongs() {
        return new Random();
    }
}
