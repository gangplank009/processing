package com.gangplank009.processing.security;

public class PrincipalsFromJwt {
    private Long id;
    private String username;

    public PrincipalsFromJwt() {
    }

    public PrincipalsFromJwt(Long id, String username) {
        this.id = id;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
