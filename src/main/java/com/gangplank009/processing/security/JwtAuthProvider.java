package com.gangplank009.processing.security;

import com.gangplank009.processing.service.JwtTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class JwtAuthProvider implements AuthenticationProvider {

    private final JwtTokenService jwtTokenService;

    public JwtAuthProvider() {
        this(null);
    }

    @Autowired
    public JwtAuthProvider(JwtTokenService jwtTokenService) {
        this.jwtTokenService = jwtTokenService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        try {
            String token = (String) authentication.getCredentials();
            String username = jwtTokenService.getUsernameFromToken(token);
            Long id = jwtTokenService.getIdFromToken(token);

            if (jwtTokenService.validateToken(token).isPresent()) {
                JwtAuthentication jwtAuthSuccess = new JwtAuthentication(token);
                jwtAuthSuccess.setAuthenticated(true);
                jwtAuthSuccess.setName(username);
                jwtAuthSuccess.setId(id);
                return jwtAuthSuccess;
            } else
                throw new JwtAuthException("JWT Token validation failed");

        } catch (JwtAuthException ex) {
            log.error(String.format("Invalid JWT Token: %s", ex.getMessage()));
            throw new JwtAuthException("Fail to verify token");
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return JwtAuthentication.class.equals(aClass);
    }
}
