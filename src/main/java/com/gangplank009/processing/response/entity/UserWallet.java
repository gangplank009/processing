package com.gangplank009.processing.response.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserWallet implements Serializable {

    private Long id;
    private Long money;

    public UserWallet(Long id, Long money) {
        this.id = id;
        this.money = money;
    }

    public static UserWallet getInstance(Long number) {
        return new UserWallet(number, number);
    }
}
