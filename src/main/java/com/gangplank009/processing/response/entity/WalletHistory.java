package com.gangplank009.processing.response.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WalletHistory implements Serializable {

    private Long id;
    private Integer codeOp;
    private Long diff;
    private LocalDateTime timestamp;

    public WalletHistory(Long id, Integer codeOp, Long diff, LocalDateTime timestamp) {
        this.id = id;
        this.codeOp = codeOp;
        this.diff = diff;
        this.timestamp = timestamp;
    }
}
