package com.gangplank009.processing.response.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


// не используется
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfo implements Serializable {

    private Long id;
    private String username;
    private String password;

    public UserInfo(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public static UserInfo getInstance(Long number) {
        return new UserInfo(number, String.valueOf(number), String.valueOf(number));
    }
}
