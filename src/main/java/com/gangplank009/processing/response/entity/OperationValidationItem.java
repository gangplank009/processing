package com.gangplank009.processing.response.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class OperationValidationItem implements Serializable {

    private Long index;
    private Boolean possibility;

    public OperationValidationItem(Long index, Boolean possibility) {
        this.index = index;
        this.possibility = possibility;
    }
}
