package com.gangplank009.processing.response.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

// не используется
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthStatus implements Serializable {

    private Boolean status;
    private String token;

    public AuthStatus(Boolean status, String token) {
        this.status = status;
        this.token = token;
    }

    public static AuthStatus getInstance(Long number) {
        return new AuthStatus(number % 2 == 0, String.valueOf(number));
    }
}