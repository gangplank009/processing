package com.gangplank009.processing.controller;

import com.gangplank009.processing.request.entity.WalletOperation;
import com.gangplank009.processing.response.entity.UserWallet;
import com.gangplank009.processing.response.entity.WalletHistory;
import com.gangplank009.processing.service.IMainProcessingService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(path = "/", produces = "application/json")
public class MainProcessingController {

    private final IMainProcessingService mainProcessingService;
    private final RestTemplate reportTemplate;
    private final RestTemplate securityTemplate;

    public MainProcessingController(IMainProcessingService mainProcessingService,
                                    @Qualifier(value = "securityTemplate") RestTemplate securityTemplate,
                                    @Qualifier(value = "reportTemplate") RestTemplate reportTemplate) {
        this.mainProcessingService = mainProcessingService;
        this.securityTemplate = securityTemplate;
        this.reportTemplate = reportTemplate;
    }

    @RequestMapping(path = "/{idUser}/wallet", method = RequestMethod.GET)
    public ResponseEntity<UserWallet> getUserWalletInfo(@PathVariable(name = "idUser") String strIdUser,
                                                        @AuthenticationPrincipal Principal principal) {

        return mainProcessingService.getUserWalletInfo(Long.valueOf(strIdUser), principal);
    }

    @RequestMapping(path = "/{idUser}/history", method = RequestMethod.GET)
    public ResponseEntity<List<WalletHistory>> getUserWalletHistory(@PathVariable(name = "idUser") String strIdUser,
                                                                    @AuthenticationPrincipal Principal principal) {

        return mainProcessingService.getUserWalletHistory(Long.valueOf(strIdUser), principal);
    }

    @RequestMapping(path = "/{idUser}/operations", method = RequestMethod.POST)
    public ResponseEntity<UserWallet> processTransferOperation(@PathVariable(name = "idUser") String strIdUser,
                                                               HttpEntity<List<WalletOperation>> operationItemList,
                                                               @AuthenticationPrincipal Principal principal) {


        return mainProcessingService.processTransferOperations(Long.valueOf(strIdUser), principal, operationItemList);

    }





}
