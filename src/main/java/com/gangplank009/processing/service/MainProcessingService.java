package com.gangplank009.processing.service;

import com.gangplank009.processing.interceptor.RestTempHeaderRequestInterceptor;
import com.gangplank009.processing.request.entity.WalletOperation;
import com.gangplank009.processing.response.entity.OperationValidationItem;
import com.gangplank009.processing.response.entity.UserWallet;
import com.gangplank009.processing.response.entity.WalletHistory;
import com.gangplank009.processing.security.JwtAuthentication;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;
import java.util.*;

@Service
public class MainProcessingService implements IMainProcessingService {

    private final RestTemplate reportWalletTemplate;

    private JwtAuthentication jwt;

    @Value(value = "${jwt.header}")
    private String STR_AUTH;

    @Value(value = "${jwt.bearer}")
    private String STR_BEARER;

    public MainProcessingService(@Qualifier(value = "reportWalletTemplate") RestTemplate reportWalletTemplate) {
        this.reportWalletTemplate = reportWalletTemplate;
    }

    @Override
    public ResponseEntity<UserWallet> getUserWalletInfo(Long idUser, Principal principal) {

        if (hasAnyErrors(idUser, principal))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();

        ResponseEntity<UserWallet> entity = reportWalletTemplate.exchange(
                "/" + idUser,
                HttpMethod.GET,
                null,
                UserWallet.class);
        return entity;
    }

    @Override
    public ResponseEntity<List<WalletHistory>> getUserWalletHistory(Long idUser, Principal principal) {

        if (hasAnyErrors(idUser, principal))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();

        return reportWalletTemplate.exchange(
                "/" + idUser + "/history",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<WalletHistory>>() {
                }
        );
    }

    @Override
    public ResponseEntity<UserWallet> processTransferOperations(Long idUser, Principal principal, HttpEntity<List<WalletOperation>> operationItemList) {
        if (hasAnyErrors(idUser, principal))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();

        ResponseEntity<List<OperationValidationItem>> validationResultList = reportWalletTemplate.exchange(
                "/" + idUser + "/valid",
                HttpMethod.POST,
                operationItemList,
                new ParameterizedTypeReference<List<OperationValidationItem>>() {
                }
        );

        if (isAllTrue(Objects.requireNonNull(validationResultList.getBody()))) {

            ResponseEntity<UserWallet> userWallet = reportWalletTemplate.exchange(
                    "/" + idUser,
                    HttpMethod.POST,
                    operationItemList,
                    UserWallet.class
            );

            return userWallet;
        } else {

            ResponseEntity<UserWallet> userWallet = getUserWalletInfo(idUser, principal);
            return new ResponseEntity<>(userWallet.getBody(), HttpStatus.BAD_REQUEST);
        }
    }


    private boolean hasAnyErrors(Long idUser, Principal principal) {
        if (false == isRequestedPathEqualsTokenUser(idUser, principal)) {
            return true;
        }
        setInterceptors();
        return false;
    }

    private boolean isRequestedPathEqualsTokenUser(Long idUser, Principal principal) {
        if (principal instanceof JwtAuthentication) {
            jwt = (JwtAuthentication) principal;
            return jwt.getId().compareTo(idUser) == 0;
        }
        return true;
    }

    private void setInterceptors() {
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        Map<String, String> headers = new HashMap<>();
        headers.put(STR_AUTH, STR_BEARER + " " + jwt.getCredentials());
        headers.put("Content-Type", "application/json");
        interceptors.clear();
        interceptors.add(new RestTempHeaderRequestInterceptor(headers));
        reportWalletTemplate.setInterceptors(interceptors);
    }

    private boolean isAllTrue(Collection<OperationValidationItem> validationItems) {
        return validationItems.stream().allMatch(OperationValidationItem::getPossibility);
    }
}