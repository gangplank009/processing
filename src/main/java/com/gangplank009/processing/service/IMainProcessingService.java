package com.gangplank009.processing.service;

import com.gangplank009.processing.request.entity.WalletOperation;
import com.gangplank009.processing.response.entity.UserWallet;
import com.gangplank009.processing.response.entity.WalletHistory;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import java.security.Principal;
import java.util.List;

public interface IMainProcessingService {
    ResponseEntity<UserWallet> getUserWalletInfo(Long idUser, Principal principal);
    ResponseEntity<List<WalletHistory>> getUserWalletHistory(Long idUser, Principal principal);
    ResponseEntity<UserWallet> processTransferOperations(Long idUser, Principal principal, HttpEntity<List<WalletOperation>> operationItemList);
}
