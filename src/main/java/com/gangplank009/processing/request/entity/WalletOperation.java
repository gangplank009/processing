package com.gangplank009.processing.request.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WalletOperation implements Serializable {

    private Long index;
    private Integer codeOp;
    private Long diff;
    private String toWalletNumber;

    public WalletOperation(Long index, Integer codeOp, Long diff, String toWalletNumber) {
        this.index = index;
        this.codeOp = codeOp;
        this.diff = diff;
        this.toWalletNumber = toWalletNumber;
    }

    public static WalletOperation getInstance(Long index, String toWalletNumber) {
        WalletOperation item = new WalletOperation();
        item.setIndex(index);
        item.setCodeOp(index % 2 == 0 ? 1 : 0);
        item.setDiff(index * 1000);
        item.setToWalletNumber(toWalletNumber);
        return item;
    }
}
